
DOCKER = docker
MAKE   = make

IMAGE_NODE       = node:12-stretch
IMAGE_SHELLCHECK = koalaman/shellcheck:stable
IMAGE_HADOLINT   = hadolint/hadolint:latest
IMAGE_FROM       = docker:latest
IMAGE_BUILT      = ci-toolkit
IMAGE_RF         = jfxs/robot-framework
IMAGE_RF_DIND    = jfxs/robot-framework-dind


## nodejs modules management
## -------------------------
yarn-install: ## Install nodejs modules with yarn. Arguments: [pull=n]
yarn-install:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache install

yarn-outdated: ## Check outdated npm packages. Arguments: [pull=n]
yarn-outdated:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache outdated || true

yarn-upgrade: ## Upgrade packages. Arguments: [pull=n]
yarn-upgrade:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache upgrade

clean-node-modules: ## Remove node_modules directory
clean-node-modules:
	rm -rf .node_cache
	rm -rf node_modules

.PHONY: yarn-install yarn-outdated yarn-upgrade clean-node-modules

## Docker
## ------
docker-build-push: ## Build docker image. Arguments: [vcs_ref=c780b3a] [tag=myTag]
docker-build-push: docker-rmi
	$(eval latest_sha := $(shell docker pull ${IMAGE_FROM} >/dev/null 2>&1 && docker inspect --format='{{index .RepoDigests 0}}' ${IMAGE_FROM}))
	@echo ${latest_sha}
	$(eval version := $(shell git fetch && git tag --sort=committerdate -l v* | tail -n1 | sed 's/^v\(.*\)$$/\1/'))
	@echo ${version}
	$(eval tag := $(shell if [ -z ${tag} ]; then echo "${IMAGE_BUILT}:latest"; else echo "${tag}"; fi))
	$(eval build_date := $(shell date -u +'%Y-%m-%dT%H:%M:%SZ'))
	$(MAKE) set-version version="${version}"
	$(DOCKER) build --no-cache --build-arg IMAGE_FROM_SHA="${latest_sha}" --build-arg RELEASE_VERSION=${version} --build-arg VCS_REF=${vcs_ref} --build-arg BUILD_DATE=${build_date} -t ${tag} .
	$(DOCKER) push ${tag}

docker-rm: ## Remove all unused containers
docker-rm:
	$(DOCKER) container prune -f

docker-rmi: ## Remove all untagged images
docker-rmi: docker-rm
	$(DOCKER) image prune -f

PHONY: docker-build-push docker-rm docker-rmi

## Tests
## ------
check: ## Run linter checks
check:
	$(DOCKER) run -t --rm -v ${PWD}:/mnt ${IMAGE_SHELLCHECK} files/*.sh
	$(DOCKER) run -t --rm -v ${PWD}:/mnt ${IMAGE_HADOLINT} hadolint /mnt/Dockerfile

rf-test-simple: ## Run robot framework simple tests
rf-test-simple:
	mkdir -p reports
	@if [ -z ${container} ]; then \
		chmod 777 reports && \
		$(DOCKER) run -t --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/files:/files:ro -v ${PWD}/reports:/reports ${IMAGE_RF} robot --include simple --outputdir /reports -v TESTS_DIR:/tests -v SHELL_DIR:/files RF && \
		chmod 755 reports ; \
	else \
		robot --include simple --outputdir reports tests/RF ; \
	fi

rf-test-root: ## Run robot framework tests as root
rf-test-root:
	mkdir -p reports
	@if [ -z ${container} ]; then \
		chmod 777 reports && \
		$(DOCKER) run -t --user root --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/files:/files:ro -v ${PWD}/reports:/reports ${IMAGE_RF} robot --include root --outputdir /reports -v TESTS_DIR:/tests -v SHELL_DIR:/files RF && \
		chmod 755 reports ; \
	else \
		robot --include root --outputdir reports tests/RF ; \
	fi

rf-test-dind: ## Run robot framework tests with docker and Dockerhub API. Arguments: repository=myRepo user=username password=password
rf-test-dind:
	test -n "${repository}"  # Failed if repository not set
	test -n "${user}"  # Failed if user not set
	test -n "${password}"  # Failed if password not set
	mkdir -p reports
	robot -v REPOSITORY:${repository} -v USERNAME:${user} -v PASSWORD:${password} --include dind --outputdir reports tests/RF

rf-test-dind-exp: ## Run robot framework tests with docker with experimental feature and Dockerhub API. Arguments: repository=myRepo user=username password=password
rf-test-dind-exp:
	test -n "${repository}"  # Failed if repository not set
	test -n "${user}"  # Failed if user not set
	test -n "${password}"  # Failed if password not set
	mkdir -p reports
	robot -v REPOSITORY:${repository} -v USERNAME:${user} -v PASSWORD:${password} --include dind-exp --outputdir reports tests/RF

rf-test-dockerhub: ## Run robot framework tests with Dockerhub API. Arguments: repository=myRepo user=username password=password
rf-test-dockerhub:
	test -n "${repository}"  # Failed if repository not set
	test -n "${user}"  # Failed if user not set
	test -n "${password}"  # Failed if password not set
	mkdir -p reports
	@if [ -z ${container} ]; then \
		chmod 777 reports && \
		$(DOCKER) run -t --user root --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/files:/files:ro -v ${PWD}/reports:/reports ${IMAGE_RF} /bin/sh -c "apk --no-cache add ca-certificates curl jq && robot -v REPOSITORY:${repository} -v USERNAME:${user} -v PASSWORD:${password} -v TESTS_DIR:/tests -v SHELL_DIR:/files --include dockerhub --outputdir /reports RF" && \
		chmod 755 reports ; \
	else \
		robot -v REPOSITORY:${repository} -v USERNAME:${user} -v PASSWORD:${password} --include dockerhub --outputdir reports tests/RF ; \
	fi

rf-test-sanity-version: ## Minimal tests to validate built image. Arguments: image=imageToTest version=1.3.2
rf-test-sanity-version:
	test -n "${image}"  # Failed if image not set
	test -n "${version}"  # Failed if version not set
	mkdir -p reports
	robot -v IMAGE:${image} -v VERSION:${version} --include sanity-version --outputdir reports tests/RF

test-in-image: ## Minimal tests to validate built image. Arguments: version=1.3.2 repository=myRepo repository2=myRepo2 user=username password=password
test-in-image:
	test -n "${version}"  # Failed if version not set
	test -n "${repository}"  # Failed if repository not set
	test -n "${repository2}"  # Failed if repository2 not set
	test -n "${user}"  # Failed if user not set
	test -n "${password}"  # Failed if password not set
	@echo "------------------ [versions] ------------------"
	@compare-versions.sh -v | grep -q ${version} && echo "compare-versions.sh: OK" || (echo "compare-versions.sh: KO" && exit 1)
	@get-identical-tag.sh -v | grep -q ${version} && echo "get-identical-tag.sh: OK" || (echo "get-identical-tag.sh: KO" && exit 1)
	@get-local-versions.sh -v | grep -q ${version} && echo "get-local-versions.sh: OK" || (echo "get-local-versions.sh: KO" && exit 1)
	@get-tag-index.sh -v | grep -q ${version} && echo "get-tag-index.sh: OK" || (echo "get-tag-index.sh: KO" && exit 1)
	@image-to-badge.sh -v | grep -q ${version} && echo "image-to-badge.sh: OK" || (echo "image-to-badge.sh: KO" && exit 1)
	@readme-to-dockerhub.sh -v | grep -q ${version} && echo "readme-to-dockerhub.sh: OK" || (echo "readme-to-dockerhub.sh: KO" && exit 1)
	@set-dockerhub-tag.sh -v | grep -q ${version} && echo "set-dockerhub-tag.sh: OK" || (echo "set-dockerhub-tag.sh: KO" && exit 1)
	@set-dockerhub-tag-multiarch.sh -v | grep -q ${version} && echo "set-dockerhub-tag-multiarch.sh: OK" || (echo "set-dockerhub-tag-multiarch.sh: KO" && exit 1)
	@versions-to-readme.sh -v | grep -q ${version} && echo "versions-to-readme.sh: OK" || (echo "versions-to-readme.sh: KO" && exit 1)
	@echo "----------------- [sanity test] -----------------"
	@compare-versions.sh -f tests/files/version_f1_ref.txt,tests/files/version_f2_ref.txt | grep -q "1" && echo "compare-versions.sh: OK" || (echo "compare-versions.sh: KO" && exit 1)
	@get-identical-tag.sh -r ${repository} -u ${user} -p ${password} -t latest -a amd64  | grep -q "3.2.1-1" && echo "get-identical-tag.sh: OK" || (echo "get-identical-tag.sh: KO" && exit 1)
	@get-local-versions.sh -f jfxs/ansible@sha256:cad01 -d /tmp && cat /tmp/VERSIONS | grep -q "jfxs/ansible@sha256:cad01" && echo "get-local-versions.sh: OK" || (echo "get-local-versions.sh: KO" && exit 1)
	@get-tag-index.sh -r ${repository} -u ${user} -p ${password} -t 1.0.1 | grep -q "4" && echo "get-tag-index.sh: OK" || (echo "get-tag-index.sh: KO" && exit 1)
	@image-to-badge.sh -i jfxs/ci-toolkit:1.1.0-1 | grep -q "size: 71 MB" && echo "image-to-badge.sh: OK" || (echo "image-to-badge.sh: KO" && exit 1)
	@readme-to-dockerhub.sh -r ${repository} -u ${user} -p ${password} -f tests/files/README_f1_ref.md && echo "readme-to-dockerhub.sh: OK" || (echo "readme-to-dockerhub.sh: KO" && exit 1)
	@set-dockerhub-tag.sh -r ${repository} -u ${user} -p ${password} -i alpine -t 3.2.1-1 && echo "set-dockerhub-tag.sh: OK" || (echo "set-dockerhub-tag.sh: KO" && exit 1)
	@set-dockerhub-tag-multiarch.sh -r ${repository2} -u ${user} -p ${password} -i ${repository2}:alpine-3.12.0-amd64 -I ${repository2}:alpine-3.12.0-arm64 -t 4.3.2-1 && echo "set-dockerhub-tag-multiarch.sh: OK" || (echo "set-dockerhub-tag-multiarch.sh: KO" && exit 1)
	@cp tests/files/README_f1_ref.md /tmp/README.md && versions-to-readme.sh -r /tmp/README.md -f tests/files/version_f1_ref.txt -p "See versions on" && cat /tmp/README.md | grep -q "3.7.3-r0" && echo "versions-to-readme.sh: OK" || (echo "versions-to-readme.sh: KO" && exit 1)
	@echo "------------------ [end tests] ------------------"

PHONY: check rf-test-simple rf-test-root rf-test-dind rf-test-dind-exp rf-test-dockerhub rf-test-sanity-version test-in-image

## Admin
## -----
import-commit-key: ## Import key for sign commit
import-commit-key:
	mkdir -p .gnupg
	$(DOCKER) run -it --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} gpg --import /root/${PRIVATE_KEY}
	chmod -R 700 .gnupg
	$(DOCKER) run -it --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} gpg --list-secret-keys --keyid-format LONG
	@echo "Run command: git config user.signingkey XXXX"
	@echo "Run command: git config commit.gpgsign true"

commit: ## Commit with Commitizen command line. Arguments: [pull=n]
commit:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -it --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} /root/yarn-commit.sh

set-version: ## Set version in shell scripts. Arguments: version=2.8.1
set-version:
	test -n "${version}"  # Failed if version not set
	@for filename in files/*.sh; do \
		sed -i 's/__VERSION__/${version}/g' "$${filename}"; \
	done

.PHONY: import-commit-key commit set-version

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
