#!/bin/sh
#
# versions-to-readme.sh is a shell program to set software versions table in a readme file.
#
# Copyright (c) 2019 FX Soubirou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VERSION=__VERSION__

README_FILE=""
VERSIONS_FILE=""
PATTERN=""
MANDATORY_ARGUMENT=1

usage() {
    echo "Usage: versions-to-readme.sh -r <readme_file> -f <versions_file> -p <pattern>"
    echo ""
    echo "Options:"
    echo "  -r Path to readme file"
    echo "  -f Path to version file"
    echo "  -p Text pattern to replace by versions table in readme file"
    echo "  -v | --version print version"
    echo "  -h print usage"
    echo ""
    echo "Example:"
    echo "versions-to-readme.sh -r README.md -f ci_versions_file_new.txt -p \"See versions on\""
}

version() {
    year=$(date +'%Y')
    echo "$0, version $VERSION"
    echo "Copyright (C) $year FX Soubirou"
    echo "License GPLv3+ : GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
}

test_argument() {
    if [ -z "$2" ]; then
        echo "Error: -$1 argument is mandatory !" >&2
        MANDATORY_ARGUMENT=0
    fi
}

if [ "$1" = "--version" ]; then
    version
    exit 0
fi

while getopts f:hp:r:v option; do
    case "${option}" in
    h)
        usage
        exit 0
        ;;
    v)
        version
        exit 0
        ;;
    r)
        README_FILE="${OPTARG}"
        ;;
    f)
        VERSIONS_FILE="${OPTARG}"
        ;;
    p)
        PATTERN="${OPTARG}"
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

test_argument "r" "$README_FILE"
test_argument "f" "$VERSIONS_FILE"
test_argument "p" "$PATTERN"

if [ "$MANDATORY_ARGUMENT" = "0" ]; then
    usage
    exit 1
fi

if ! [ -e "$README_FILE" ]; then
    echo "Error: readme file $README_FILE does not exist!" >&2
    exit 1
fi
if ! [ -e "$VERSIONS_FILE" ]; then
    echo "Error: versions file $VERSIONS_FILE does not exist!" >&2
    exit 1
fi

if ! grep -q "$PATTERN" "$README_FILE"; then
    echo "Error: pattern $PATTERN does not exist in file $README_FILE!" >&2
    exit 1
fi

TITLE="| Software | Version |"
TITLE_SEP="|----------|---------|"

sed -i "s/^$PATTERN.*/$TITLE/g" "$README_FILE"
sed -i "/^$TITLE/a $TITLE_SEP" "$README_FILE"

prev_table_line=$TITLE_SEP

while IFS='=' read -r software version
do
    clean_version=$(echo "$version" | cut -f 1 -d "|" | tr -cd '\40-\176')
    length=${#clean_version}
    if [ "$length" -gt "22" ]; then
        cut_version=$(echo "$clean_version" | cut -c1-22 | sed 's/$/ .../')
    else
        cut_version=$clean_version
    fi
    table_line=$(printf "| %s | %s |\\n" "$software" "$cut_version")
    sed -i.bu "/^$prev_table_line/a $table_line" "$README_FILE"
    prev_table_line=$table_line
done <"$VERSIONS_FILE"
