#!/bin/sh
#
# get-identical-tag.sh is a shell program to get a list of tag image with the same digest.
#
# Copyright (c) 2020 FX Soubirou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# get-identical-tag.sh uses the Open Source/Free Software "curl" by Daniel Stenberg
# distributed under the MIT/X derivate license:
# https://curl.haxx.se/docs/copyright.html

VERSION=__VERSION__

HUB_ENDPOINT="https://hub.docker.com/v2"

REPO_NAME=""
USERNAME=""
PASSWORD=""
TAG_VERSION=""
ARCHITECTURE=""
REGEX=""
MANDATORY_ARGUMENT=1

usage() {
    echo "This script gets a list of tag image with the same digest and"
    echo "optionaly extracts a substring of a matching tag with a regex pattern."
    echo "The output has the following format:"
    echo "sha256_digest tags_list_separated_by_comma [regex_output]"
    echo ""
    echo "Usage: get-identical-tag.sh -r <repository> -u <username> -p <password> -t <tag-version> -a <architecture> [-x <regex>]"
    echo "curl and jq are mandatory"
    echo ""
    echo "Options:"
    echo "  -r Repository. Official repository starts with library. Example: library/nginx"
    echo "  -u Username of repository admin account"
    echo "  -p Password of repository admin account"
    echo "  -t Tag"
    echo "  -a Architecture: amd64, arm, arm64, 386 ..."
    echo "  -x Regex to get a substring of a matching tag of the tags list"
    echo "  -v | --version print version"
    echo "  -h print usage"
    echo ""
    echo "Example:"
    echo "get-identical-tag.sh -r library/nginx -u dockerhubUser -p dockerhubPassword -t latest -a amd64 returns:"
    echo "sha256:0efad4d09a419dc6d574c3c3baacb804a530acd61d5eba72cb1f14e1f5ac0c8f latest,mainline,1.19.0,1.19,1"
    echo "with -x option:"
    echo "get-identical-tag.sh -r library/nginx -u dockerhubUser -p dockerhubPassword -t stable-alpine -a arm64 -x \"\([0-9]*\.[0-9]*\.[0-9]*\)-alpine\" returns:"
    echo "sha256:676b8117782d9e8c20af8e1b19356f64acc76c981f3a65c66e33a9874877892a stable-alpine,1.18.0-alpine,1.18-alpine 1.18.0"
    echo "get-identical-tag.sh -r jfxs/ci-toolkit -u dockerhubUser -p dockerhubPassword -t latest -a amd64 -x \"\([0-9]*.[0-9]*.[0-9]\)-[0-9]*\" returns:"
    echo "sha256:071939073647305a2b519cb5e83b6f90f3178d812f563f56eaed53b96147f726 latest,2.2,2,2.2.1-2 2.2.1"
}

version() {
    year=$(date +'%Y')
    echo "$0, version $VERSION"
    echo "Copyright (C) $year FX Soubirou"
    echo "License GPLv3+ : GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
    echo "get-identical-tag.sh uses the Open Source/Free Software \"curl\" by Daniel Stenberg"
    echo "distributed under the MIT/X derivate license:"
    echo "https://curl.haxx.se/docs/copyright.html"
}

test_argument() {
    if [ -z "$2" ]; then
        echo "Error: -$1 argument is mandatory !" >&2
        MANDATORY_ARGUMENT=0
    fi
}

if [ "$1" = "--version" ]; then
    version
    exit 0
fi

while getopts a:hp:r:t:u:vx: option; do
    case "${option}" in
    h)
        usage
        exit 0
        ;;
    v)
        version
        exit 0
        ;;
    r)
        REPO_NAME="${OPTARG}"
        ;;
    u)
        USERNAME="${OPTARG}"
        ;;
    p)
        PASSWORD="${OPTARG}"
        ;;
    t)
        TAG_VERSION="${OPTARG}"
        ;;
    a)
        ARCHITECTURE="${OPTARG}"
        ;;
    x)
        REGEX="${OPTARG}"
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

test_argument "r" "$REPO_NAME"
test_argument "u" "$USERNAME"
test_argument "p" "$PASSWORD"
test_argument "t" "$TAG_VERSION"
test_argument "a" "$ARCHITECTURE"

if [ "$MANDATORY_ARGUMENT" = "0" ]; then
    usage
    exit 1
fi

# Get auth token
token_response=$(curl -w "%{http_code}" -s -H "Content-Type: application/json" -X POST -d "{\"username\": \"${USERNAME}\", \"password\": \"${PASSWORD}\"}" "${HUB_ENDPOINT}"/users/login/)
http_code=$(echo "$token_response" | tr -d '\n' | tail -c 3)
if [ "${http_code}" != "200" ]; then
    printf "Unable to login to Dockerhub, response code: %s\\n" "${http_code}" >&2
    exit 1
fi
token=$(echo "$token_response" | tr -d '\n' | sed "s/^\\({.*}\\).*/\\1/g" | jq -r .token)

# Get tags list from Dockerhub
tags_response=$(curl -w "%{http_code}" -s -H "Authorization: JWT ${token}" "${HUB_ENDPOINT}/repositories/${REPO_NAME}/tags/?page_size=10000")
http_code=$(echo "$tags_response" | tr -d '\n' | tail -c 3)
if [ "${http_code}" != "200" ]; then
    printf "Unable to get tags list for %s on Dockerhub, response code: %s\\n" "${REPO_NAME}" "${http_code}" >&2
    exit 1
fi

tags_info=$(echo "$tags_response" | tr -d '\n' | sed "s/^\\({.*}\\).*/\\1/g" | jq -r '.results|.[]| "\(.name) \(.images[])"')

output_tag_digest=""
digest_tag_list=""

# Create a list "digest tag" for the specified architecture
while IFS= read -r line;
do
    architecture=$(echo "$line" | awk '{print $2;}' | sed -E 's/.*"architecture":"?([^,"]*)"?.*/\1/');
    if [ "$ARCHITECTURE" = "$architecture" ]; then
        tag=$(echo "$line" | awk '{print $1;}');
        digest=$(echo "$line" | awk '{print $2;}' | sed -E 's/.*"digest":"?([^,"]*)"?.*/\1/');
        digest_first_char=$(echo "$digest" | head -c 3)
        if [ "$digest_first_char" = "sha" ]; then
            if [ "$tag" = "$TAG_VERSION" ]; then
                output_tag_digest="$digest"
            fi
            digest_tag_list="$digest_tag_list $digest|$tag";
        fi
    fi
done <<EOF
$tags_info
EOF

if [ -z "$output_tag_digest" ]; then
    echo "Error: Tag $TAG_VERSION not found for architecture $ARCHITECTURE" >&2
    exit 1
fi

output_tag_list="$TAG_VERSION"

for line in $digest_tag_list
do
    digest=$(echo "$line" | sed 's/|.*//');
    if [ "$digest" = "$output_tag_digest" ]; then
        tag=$(echo "$line" | sed 's/.*|//');
        if [ "$tag" != "$TAG_VERSION" ]; then
            output_tag_list="$output_tag_list,$tag"
        fi
    fi
done

if [ -n "$REGEX" ]; then
    output_subtag=""

    IFS=","
    for tag in $output_tag_list
    do
        if expr "$tag" : "$REGEX" 1>/dev/null; then
            output_subtag=$(echo "$tag" | sed "s/$REGEX/\1/");
        fi
    done
    if [ -z "$output_subtag" ]; then
        echo "Error: Regex \"$REGEX\" not found in list: $output_tag_list" >&2
        exit 1
    fi
    echo "$output_tag_digest $output_tag_list $output_subtag"
else
    echo "$output_tag_digest $output_tag_list"
fi
