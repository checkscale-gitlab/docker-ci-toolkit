#!/bin/sh
#
# get-local-versions.sh is a shell program to generate a file with versions of installed packages.
#
# Copyright (c) 2019 FX Soubirou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VERSION=__VERSION__

SEP="|"
FROM_DIGEST=""
ALPINE_PACKAGES_ARG=""
PYTHON_PACKAGES_ARG=""
CLI_PACKAGES_ARG=""
SPECIFIC_PACKAGES_ARG=""
OS_RELEASE_FILE="/etc/os-release"
FILE="/etc/VERSIONS"

usage() {
    echo "Usage: get-local-versions.sh -f <from-image-digest> [-a <alpine-package1>,<alpine-package2>] [-p <python-package1>,<python-package2>] [-c <command1>,<command2>] [-s <package1=version1>,<package2=version2>] [-d <versions-directory>]"
    echo ""
    echo "Options:"
    echo "  -f From image digest"
    echo "  -a List of alpine packages separated by comma"
    echo "  -p List of python packages separated by comma"
    echo "  -c List of commands separated by comma. Version is set by the output of --version option of the cli"
    echo "  -s List of specific packages with version separated by comma"
    echo "  -d Directory in which the VERSIONS file is created. /etc by default"
    echo "  -v | --version print version"
    echo "  -h print usage"
    echo ""
    echo "Example:"
    echo "get-local-versions.sh -f \${IMAGE_FROM_SHA} -c docker -s ci-toolkit=\${RELEASE_VERSION}"
    echo "get-local-versions.sh -f \${IMAGE_FROM_SHA} -a python3 -p ansible-base,ansible-lint,jmespath,netaddr"
}

version() {
    year=$(date +'%Y')
    echo "$0, version $VERSION"
    echo "Copyright (C) $year FX Soubirou"
    echo "License GPLv3+ : GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
}

get_alpine_package_version() {
    apk info -d "$1" | head -n 1 | awk '{print $1;}' | sed -e "s/^$1-//"
}

get_python_package_version() {
    pip show "$1" | grep Version | awk '{print $2;}'
}

get_cli_package_version() {
    if ! [ -x "$(command -v "$1")" ]; then
        echo "ERROR_CMD_NOT_FOUND"
    else
        output=$("$1" --version)
        echo "$output" | head -n 1 | sed 's/[^0-9]*\([0-9.]*\).*/\1/'
    fi
}

if [ "$1" = "--version" ]; then
    version
    exit 0
fi

while getopts a:c:d:f:hp:s:v option; do
    case "${option}" in
    h)
        usage
        exit 0
        ;;
    v)
        version
        exit 0
        ;;
    f)
        FROM_DIGEST="${OPTARG}"
        ;;
    a)
        ALPINE_PACKAGES_ARG="${OPTARG}"
        ;;
    p)
        PYTHON_PACKAGES_ARG="${OPTARG}"
        ;;
    c)
        CLI_PACKAGES_ARG="${OPTARG}"
        ;;
    s)
        SPECIFIC_PACKAGES_ARG="${OPTARG}"
        ;;
    d)
        FILE="${OPTARG}/VERSIONS"
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

if [ -z "$FROM_DIGEST" ]; then
    echo "Error: -f argument is mandatory !" >&2
    usage
    exit 1
fi

echo "from=${FROM_DIGEST}${SEP}F" >"$FILE"

grep VERSION_ID "${OS_RELEASE_FILE}" | sed -e "s/^VERSION_ID/alpine/" | sed 's/$/|O/' >>"$FILE"

if [ -n "$ALPINE_PACKAGES_ARG" ]; then
    apk update
fi

IFS=","
for package in $ALPINE_PACKAGES_ARG
do
   version=$(get_alpine_package_version "$package")
   echo "${package}=${version}${SEP}A" >>"$FILE"
done

for package in $PYTHON_PACKAGES_ARG
do
   version=$(get_python_package_version "$package")
   echo "${package}=${version}${SEP}P" >>"$FILE"
done

for package in $CLI_PACKAGES_ARG
do
   version=$(get_cli_package_version "$package")
   echo "${package}=${version}${SEP}C" >>"$FILE"
done

for package in $SPECIFIC_PACKAGES_ARG
do
   echo "${package}${SEP}S" >>"$FILE"
done

if [ -n "$ALPINE_PACKAGES_ARG" ]; then
    rm -rf /var/cache/apk/*
fi
