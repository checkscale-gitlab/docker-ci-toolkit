#!/bin/sh
#
# set-dockerhub-tag-multiarch.sh is a shell program to publish images on Dockerhub with specific tags
# for architecture amd64 and arm64.
#
# Copyright (c) 2020 FX Soubirou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# set-dockerhub-tag-multiarch.sh uses the Open Source/Free Software "curl" by Daniel Stenberg
# distributed under the MIT/X derivate license:
# https://curl.haxx.se/docs/copyright.html

set -e

VERSION=__VERSION__

HUB_ENDPOINT="https://hub.docker.com/v2"

USERNAME=""
PASSWORD=""
IMAGE_AMD=""
IMAGE_ARM=""
REPOSITORY=""
TAG=""
LATEST=0
MAJOR=0
MINOR=0
MANDATORY_ARGUMENT=1

usage() {
    echo "Usage: set-dockerhub-tag-multiarch.sh -u <username> -p <password> -i <source-image-amd64> -I <source-image-arm64> -r <image-repository> -t <tag> [-l] [-M] [-m]"
    echo "curl and jq are mandatory"
    echo ""
    echo "Options:"
    echo "  -u username of repository admin account"
    echo "  -p password of repository admin account"
    echo "  -i source image amd64"
    echo "  -I source image arm64"
    echo "  -r image repository"
    echo "  -t tag"
    echo "  -l add tag latest"
    echo "  -M add major version tag: 3.2.1-4 -> tag 3"
    echo "  -m add minor version tag: 3.2.1-4 -> tag 3.2"
    echo "  -v | --version print version"
    echo "  -h print usage"
    echo ""
    echo "Example with images built and pushed on gitlab.com registry:"
    echo "set-dockerhub-tag-multiarch.sh -u dockerhubUser -p dockerhubPassword \\"
    echo "-i registry.gitlab.com/fxs/docker-hello-world/master-amd64:499694f5 -I registry.gitlab.com/fxs/docker-hello-world/master-arm64:499694f5 \\"
    echo "-r jfxs/hello-world -t 1.19.3-2 -l -M -m"
}

version() {
    year=$(date +'%Y')
    echo "$0, version $VERSION"
    echo "Copyright (C) $year FX Soubirou"
    echo "License GPLv3+ : GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
    echo "set-dockerhub-tag-multiarch.sh uses the Open Source/Free Software \"curl\" by Daniel Stenberg"
    echo "distributed under the MIT/X derivate license:"
    echo "https://curl.haxx.se/docs/copyright.html"
}

test_argument() {
    if [ -z "$2" ]; then
        echo "Error: -$1 argument is mandatory !" >&2
        MANDATORY_ARGUMENT=0
    fi
}

docker_push() {
    if ! docker push "$1"; then
        echo "Impossible to push image $1" >&2
        exit 1
    fi
}

docker_manifest_create() {
if ! docker manifest create "$1" --amend "$REPOSITORY:$TAG-tmp-amd64" --amend "$REPOSITORY:$TAG-tmp-arm64"; then
    echo "Error with docker manifest. Is experimental features enabled?" >&2
    exit 1
fi
}

docker_manifest_push() {
    if ! docker manifest push "$1"; then
        echo "Impossible to push image $1 with docker manifest push" >&2
        exit 1
    fi
}

if [ "$1" = "--version" ]; then
    version
    exit 0
fi

while getopts hi:I:lMmp:r:s:t:u:v option; do
    case "${option}" in
    h)
        usage
        exit 0
        ;;
    v)
        version
        exit 0
        ;;
    u)
        USERNAME="${OPTARG}"
        ;;
    p)
        PASSWORD="${OPTARG}"
        ;;
    i)
        IMAGE_AMD="${OPTARG}"
        ;;
    I)
        IMAGE_ARM="${OPTARG}"
        ;;
    r)
        REPOSITORY="${OPTARG}"
        ;;
    t)
        TAG="${OPTARG}"
        ;;
    l)
        LATEST=1
        ;;
    M)
        MAJOR=1
        ;;
    m)
        MINOR=1
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

test_argument "u" "$USERNAME"
test_argument "p" "$PASSWORD"
test_argument "i" "$IMAGE_AMD"
test_argument "I" "$IMAGE_ARM"
test_argument "r" "$REPOSITORY"
test_argument "t" "$TAG"

if [ "$MANDATORY_ARGUMENT" = "0" ]; then
    usage
    exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
    echo "docker command not found" >&2
    exit 1
fi

if ! docker pull "$IMAGE_AMD"; then
    echo "Impossible to pull source image: $IMAGE_AMD" >&2
    exit 1
fi

if ! docker pull "$IMAGE_ARM"; then
    echo "Impossible to pull source image: $IMAGE_ARM" >&2
    exit 1
fi

if ! docker tag "$IMAGE_AMD" "$REPOSITORY:$TAG-tmp-amd64"; then
    echo "Impossible to tag image" >&2
    exit 1
fi

if ! docker tag "$IMAGE_ARM" "$REPOSITORY:$TAG-tmp-arm64"; then
    echo "Impossible to tag image" >&2
    exit 1
fi

if ! docker login -u "$USERNAME" -p "$PASSWORD"; then
    echo "Impossible to login to Dockerhub" >&2
    exit 1
fi

docker_push "$REPOSITORY:$TAG-tmp-amd64"
docker_push "$REPOSITORY:$TAG-tmp-arm64"

docker_manifest_create "$REPOSITORY:$TAG"

docker_manifest_push "$REPOSITORY:$TAG"

if [ "$LATEST" = 1 ]; then
    docker_manifest_create "$REPOSITORY:latest"
    docker_manifest_push "$REPOSITORY:latest"
fi

if [ "$MAJOR" = 1 ]; then
    major_version=$(echo "$TAG" | sed 's/[^0-9.]*\([0-9]*\).*/\1/')
    if [ -z "$major_version" ]; then
        echo "Invalid tag for major version" >&2
        exit 1
    else
        docker_manifest_create "$REPOSITORY:$major_version"
        docker_manifest_push "$REPOSITORY:$major_version"
    fi
fi

if [ "$MINOR" = 1 ]; then
    minor_version=$(echo "$TAG" | sed 's/[^0-9.]*\([0-9]*.[0-9]*\).*/\1/')
    if [ -z "$minor_version" ]; then
        echo "Invalid tag for minor version" >&2
        exit 1
    else
        docker_manifest_create "$REPOSITORY:$minor_version"
        docker_manifest_push "$REPOSITORY:$minor_version"
    fi
fi

# Get auth token
token_response=$(curl -w "%{http_code}" -s -H "Content-Type: application/json" -X POST -d "{\"username\": \"${USERNAME}\", \"password\": \"${PASSWORD}\"}" "${HUB_ENDPOINT}"/users/login/)
http_code=$(echo "$token_response" | tr -d '\n' | tail -c 3)
if [ "${http_code}" != "200" ]; then
    printf "Unable to login to Dockerhub, response code: %s\\n" "${http_code}" >&2
    exit 1
fi
token=$(echo "$token_response" | tr -d '\n' | sed "s/^\\({.*}\\).*/\\1/g" | jq -r .token)

# Delete tmp images
tags_response=$(curl -w "%{http_code}" -s -H "Authorization: JWT ${token}" -X DELETE "${HUB_ENDPOINT}/repositories/${REPOSITORY}/tags/$TAG-tmp-amd64/")
http_code=$(echo "$tags_response" | tr -d '\n' | tail -c 3)
if [ "${http_code}" != "204" ] && [ "${http_code}" != "404" ]; then
    printf "Unable to delete the image %s on Dockerhub, response code: %s\\n" "${REPOSITORY}/$TAG-tmp-amd64" "${http_code}" >&2
    exit 1
fi

tags_response=$(curl -w "%{http_code}" -s -H "Authorization: JWT ${token}" -X DELETE "${HUB_ENDPOINT}/repositories/${REPOSITORY}/tags/$TAG-tmp-arm64/")
http_code=$(echo "$tags_response" | tr -d '\n' | tail -c 3)
if [ "${http_code}" != "204" ] && [ "${http_code}" != "404" ]; then
    printf "Unable to delete the image %s on Dockerhub, response code: %s\\n" "${REPOSITORY}/$TAG-tmp-arm64" "${http_code}" >&2
    exit 1
fi
