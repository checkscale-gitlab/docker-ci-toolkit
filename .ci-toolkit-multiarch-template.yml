variables:
  T_CI_REGISTRY_IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG-$ARCH:$CI_COMMIT_SHORT_SHA
  T_CI_REGISTRY_IMAGE_TAG_AMD64: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG-amd64:$CI_COMMIT_SHORT_SHA
  T_CI_REGISTRY_IMAGE_TAG_ARM64: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG-arm64:$CI_COMMIT_SHORT_SHA
  T_DOCKERHUB_IMAGE_DEFAULT_TAG: latest
  T_DOCKERHUB_REGISTRY: docker.io
  T_IMAGE_BUILDX_QEMU: jonoh/docker-buildx-qemu
  T_IMAGE_CI_TOOLKIT: jfxs/ci-toolkit:latest
  T_IMAGE_DOCKER: docker:latest
  T_IMAGE_HADOLINT: hadolint/hadolint:latest-debian
  T_README_FILE: README.md
  T_README_VERSIONS_PATTERN: "See versions on"
  T_SERVICE_DOCKER_TAG: stable-dind

stages:
  - checks
  - build-image
  - get-versions
  - dockerhub-tag
  - dockerhub-doc
  - gitlab-pages

checks:
  image: $T_IMAGE_HADOLINT
  stage: checks
  script:
    - hadolint Dockerfile

amd64:build:
  variables:
    ARCH: amd64
  retry: 2
  image: $T_IMAGE_DOCKER
  stage: build-image
  services:
    - docker:$T_SERVICE_DOCKER_TAG
  before_script:
    - apk --no-cache add curl git jq make
    - echo -n "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
  script:
    - make docker-build-push user=$T_DOCKERHUB_USER password=$T_DOCKERHUB_PASSWORD arch=$ARCH vcs_ref="$CI_COMMIT_SHORT_SHA" tag=$T_CI_REGISTRY_IMAGE_TAG
    - docker inspect --format '{{ index .Config.Labels "org.opencontainers.image.version"}}' $T_CI_REGISTRY_IMAGE_TAG | tee ci_image_version.$ARCH
  artifacts:
    paths:
    - ci_image_version.$ARCH
    expire_in: 1 week

arm64:build:
  extends: amd64:build
  variables:
    ARCH: arm64
    DOCKER_HOST: tcp://docker:2375/
  image: $T_IMAGE_BUILDX_QEMU
  before_script:
    - apt-get update && apt-get install -y curl git jq make
    - echo -n "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
    - update-binfmts --enable
    - docker buildx create --driver docker-container --use
    - docker buildx inspect --bootstrap
    - docker buildx ls

amd64:get-versions:
  variables:
    ARCH: amd64
  retry: 2
  image: $T_IMAGE_DOCKER
  stage: get-versions
  services:
    - docker:$T_SERVICE_DOCKER_TAG
  script:
     - docker run -t $T_DOCKERHUB_REGISTRY/$T_DOCKERHUB_IMAGE_REPOSITORY:latest cat /etc/VERSIONS > ci_versions_file_latest.$ARCH || touch ci_versions_file_latest.$ARCH
     - docker run -t $T_CI_REGISTRY_IMAGE_TAG cat /etc/VERSIONS > ci_versions_file_new.$ARCH
  artifacts:
    paths:
    - ci_versions_file_latest.$ARCH
    - ci_versions_file_new.$ARCH
    expire_in: 1 week

arm64:get-versions:
  extends: amd64:get-versions
  tags:
    - arm64
  variables:
    ARCH: arm64

dockerhub-tag:
  only:
    refs:
      - master
  retry: 2
  image: $T_IMAGE_CI_TOOLKIT
  stage: dockerhub-tag
  dependencies:
    - amd64:build
    - arm64:build
    - amd64:get-versions
    - arm64:get-versions
  services:
    - name: docker:$T_SERVICE_DOCKER_TAG
      command: ["--experimental"]
  before_script:
    - if cmp ci_image_version.amd64 ci_image_version.arm64 >/dev/null 2>&1; then
        image_version=$(cat ci_image_version.amd64);
        echo "Image version $image_version";
      else
        echo "Problem! files ci_image_version.amd64 and ci_image_version.arm64 are different";
        exit 1;
      fi
    - if [ -z ${SCHEDULE} ]; then
        is_new_tag_amd64=$(compare-versions.sh -f ci_versions_file_latest.amd64,ci_versions_file_new.amd64 -g Dockerfile);
        is_new_tag_arm64=$(compare-versions.sh -f ci_versions_file_latest.arm64,ci_versions_file_new.arm64 -g Dockerfile);
      else
        is_new_tag_amd64=$(compare-versions.sh -f ci_versions_file_latest.amd64,ci_versions_file_new.amd64);
        is_new_tag_arm64=$(compare-versions.sh -f ci_versions_file_latest.arm64,ci_versions_file_new.arm64);
      fi
    - if [ $is_new_tag_amd64 -eq 0 ] && [ $is_new_tag_arm64 -eq 0 ]; then
        is_new_tag=0;
      else
        is_new_tag=1;
      fi
    - echo $is_new_tag | tee ci_is_new_tag
    - if [ "$is_new_tag" = 1 ]; then versions-to-readme.sh -r $T_README_FILE -f ci_versions_file_new.amd64 -p $T_README_VERSIONS_PATTERN; fi
  script:
    - if [ "$is_new_tag" = 1 ]; then index=$(get-tag-index.sh -r $T_DOCKERHUB_IMAGE_REPOSITORY -u $T_DOCKERHUB_USER -p $T_DOCKERHUB_PASSWORD -t $image_version); fi
    - if [ "$is_new_tag" = 1 ]; then set-dockerhub-tag-multiarch.sh -u $T_DOCKERHUB_USER -p $T_DOCKERHUB_PASSWORD -i $T_CI_REGISTRY_IMAGE_TAG_AMD64 -I $T_CI_REGISTRY_IMAGE_TAG_ARM64 -r $T_DOCKERHUB_IMAGE_REPOSITORY -t $image_version-$index -l -M -m; fi
    - if [ "$is_new_tag" = 1 ]; then echo $image_version-$index | tee ci_tag_index; else touch ci_tag_index; fi
    - if [ "$is_new_tag" = 1 ]; then readme-to-dockerhub.sh -r $T_DOCKERHUB_IMAGE_REPOSITORY -u $T_DOCKERHUB_USER -p $T_DOCKERHUB_PASSWORD -f $T_README_FILE; fi
  artifacts:
    paths:
    - ci_is_new_tag
    - ci_tag_index
    expire_in: 1 week

dockerhub-doc:
  only:
    changes:
      - README.md
    refs:
      - master
  except:
    - schedules
  retry: 2
  image: $T_IMAGE_CI_TOOLKIT
  stage: dockerhub-doc
  dependencies:
    - amd64:get-versions
    - dockerhub-tag
  before_script:
    - is_new_tag=$(cat ci_is_new_tag)
    - echo "$is_new_tag"
  script:
    - if [ "$is_new_tag" -ne 1  ]; then versions-to-readme.sh -r $T_README_FILE -f ci_versions_file_latest.amd64 -p $T_README_VERSIONS_PATTERN; fi
    - if [ "$is_new_tag" -ne 1  ]; then readme-to-dockerhub.sh -r $T_DOCKERHUB_IMAGE_REPOSITORY -u $T_DOCKERHUB_USER -p $T_DOCKERHUB_PASSWORD -f $T_README_FILE; fi

pages:
  only:
    refs:
      - master
  image: $T_IMAGE_CI_TOOLKIT
  stage: gitlab-pages
  services:
    - docker:$T_SERVICE_DOCKER_TAG
  script:
    - image-to-badge.sh -i $T_DOCKERHUB_IMAGE_REPOSITORY:$T_DOCKERHUB_IMAGE_DEFAULT_TAG -d public
  artifacts:
    paths:
      - public
  when: always
