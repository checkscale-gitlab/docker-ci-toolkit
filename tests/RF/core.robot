** Settings ***
Documentation     core tests
...
...               Test to validate Robot Framework.

Library           Process

*** Variables ***

*** Test Cases ***
Test Robot Framework: [--version] option
    [Tags]    core    simple
    ${result} =    When Run Process    robot   --version
    Then Should Be Equal As Integers    ${result.rc}    251
    And Should Contain    ${result.stdout}    Robot Framework
