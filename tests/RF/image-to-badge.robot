** Settings ***
Documentation     image-to-badge.sh tests
...
...               A test suite to validate image-to-badge shell script.

Library           OperatingSystem
Library           Process
Library           String

*** Variables ***
${TESTS_DIR}   tests
${SHELL_DIR}   files
${SHELL.SH}   image-to-badge.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} -i
${BADGE}      docker.svg
${IMAGE}     jfxs/ci-toolkit
${TAG}        1.1.0-1
${REF_SIZE}     71 MB
${REF_LAYERS}    24

*** Test Cases ***
with no option
    [Tags]    image-to-badge    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -i argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

with unknown option
    [Tags]    image-to-badge    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -x
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Illegal option -x
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-i] option missing
    [Tags]    image-to-badge    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   /tmp
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -i argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-i] option with missing argument
    [Tags]    image-to-badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   /tmp   -i
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-d] option with missing argument
    [Tags]    image-to-badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -i   ${IMAGE}:${TAG}   -d
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-v] option
    [Tags]    image-to-badge    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[--version] option
    [Tags]    image-to-badge    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[-h] option
    [Tags]    image-to-badge    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

Docker command not found
    [Tags]    image-to-badge    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -i   ${IMAGE}:${TAG}   -d   /tmp
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    docker command not found

Source image not found
    [Tags]    image-to-badge    dind
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -i   wrongimage   -d   /tmp
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Impossible to pull image

Generated badge shows size and layers
    [Tags]    image-to-badge    dind
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -i   ${IMAGE}:${TAG}   -d   /tmp
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    size: ${REF_SIZE}
    And Should Contain    ${result.stdout}    layers: ${REF_LAYERS}
    And badge displays   /tmp/${BADGE}    ${REF_SIZE}    ${REF_LAYERS}

*** Keywords ***
badge displays
    [Arguments]    ${file}    ${size}    ${layers}
    File Should Exist   ${file}
    ${content} =   Get File    ${file}
    Should Contain   ${content}    ${size}
    Should Contain   ${content}    ${layers}
